'use strict';

module.exports = randomInt;
const names = require('./faces-book.js');

function randomInt(lo,hi) {
  return Math.floor(Math.random()*(hi - lo)) + lo;
}
let shuffnames = names;
let temp;
let rand = Math.floor(Math.random()*27) + 1;
for (let i = 27;i>=1;i--){
  for(let j = rand;j>=i>=0;j++){
    temp=shuffnames[j];
    shuffnames[j]=names[i];
    names[i]=temp;
  }
}
