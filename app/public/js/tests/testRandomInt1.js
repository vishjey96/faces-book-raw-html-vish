'use strict';

module.exports = testRandomInt;

const randomInt = require('./randomInt.js');
const assert = require('assert');


function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_randomInt_for_all_numbers();
}

function test_randomInt_with_lower_bound_included() {
 let count = 0;
 for(let i=0;i<1000;i++){
  if(randomInt(1,10)===1){
    count=1;
    break;
  }
 }
  if(count===0){
    assert.equal(1,2);
  }
}

function test_randomInt_with_upper_bound_excluded() {
  let count = 0;
  for(let i=0;i<1000;i++){
  if(randomInt(1,10)!==10){
     count=1;
     break;
  }
 }
  if(count===0) {
    assert.equal(1,2);
  }
}
function test_randomInt_for_all_numbers(){
  let count1=0,count2=0,count3=0,count4=0;
  for(let i=0;i<1000;i++){
    switch(randomInt(1,5)){
      case 1:
        count1++; break;
      case 2:
        count2++;break;
      case 3:
        count3++;break;
      case 4:
        count4++;break;
      }
  }
  assert.ok(count1 > 0);
  assert.ok(count2 > 0);
  assert.ok(count3 > 0);
  assert.ok(count4 > 0);
}


testRandomInt();
console.log("All tests passed");
