$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded
const faces = $('faces');
const makeFace = function(name){
  return $('<img>',{
    class:"face",
   src:`img/2018/${name}.jpg`,
   title:name
});
};
const names = [
    "Akshat",
    "Karthika",
    "Sanjana",
    "Mahidher",
    "Akhil",
    "Keerthana",
    "RVishnuPriya",
    "Anushree",
    "Mariah",
    "Aishu",
    "Rishi",
    "Jon",
    "Pavithrann",
    "Sindhu",
    "Rishi",
    "Supriya",
    "Shruti",
    "Santhosh",
    "Supriya",
    "Gayatri",
    "Vishnu",
    "Varsha",
    "John",
    "Ameya",
    "Sudeep",
    "Janani",
    "Thamizh",
    "Thiru",
];

names.forEach(function(name){
faces.append(makeFace(name));
});
});
